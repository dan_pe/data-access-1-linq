﻿using System;
using DataAccess;

namespace _1_LinqFilteringAndOrdering
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataService = new DataService();

            // prevent closing the console
            Console.ReadLine();
        }
    }
}


